## Recipes Website

I've completed the first feature ```recipe_list.feature```.


I had to add some ```Given``` statements to the feature file as they seemed to be missing from the original document. (Maybe that was part of the test...)


I've implemented the recipe website as two projects:

* Recipes API
* Recipes Web

I started looking at the second ```recipe.feature```. The feature should work if you click around the web site, but it's still WIP / Scratch work and not covered fully with tests.


## Running the Project
I've added a few scripts to the project so hopefully it should be straight forward to get up and running.

```npm start``` should spin up both web and api projects and launch a web browser to display the web front end.

```sh
npm run install
npm start
```

I'm on a mac and haven't tested that it works on windows. I think it should be ok, but I've not had time to test it in a windows environment

### Running the BDD Feature Tests
```sh
npm run test:cucumber
```

### Running the API Tests
```sh
npm run test:api
```

