import 'whatwg-fetch';

export function fetchRecipes() {
  console.log('fetching recipes');
  return fetch('/api/recipes').then(response => response.json())
}

export function fetchRecipe(recipeName) {
  console.log('fetching recipe', recipeName);
  return fetch(`/api/recipe/${ recipeName }`)
    .then(response => response.json())
    .catch(err => {
      console.log(err.message);
      return Promise.resolve();
    });
}
