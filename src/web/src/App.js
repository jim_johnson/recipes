import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import RecipesComponent from "./components/Recipes";
import RecipeComponent from "./components/Recipe"
import { fetchRecipe, fetchRecipes } from './data/fetchRecipes';
import './App.css';
import withPrefetch from './components/withPrefetchHOC';

const Recipes = withPrefetch(RecipesComponent, () => fetchRecipes(), 'recipes');

const Recipe = withPrefetch(RecipeComponent, (props) => {
  const recipeName = props.match.params.recipeName;
  return fetchRecipe(recipeName);
}, 'recipe');

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path='/' component={ Recipes } />
          <Route exact path='/recipe/:recipeName' component={ Recipe } />
        </div>
      </Router>
    );
  }
}

export default App;
