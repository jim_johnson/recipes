import React from 'react';
import { Link } from 'react-router-dom';

export default function Recipes({recipes}) {
  return (
    <div className={'recipes'}>
      <h1>Recipes</h1>
      { recipes.length ? <RecipeList recipes={recipes} /> : <NoRecipes /> }
    </div>
  )
}

function NoRecipes() {
  return <p>Sorry, we currently have no recipes for you</p>
}

function TableHeader() {
  return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Cooking Time</th>
          <th>Ingredients</th>
        </tr>
      </thead>
  );
}

function RecipeList({recipes}) {
  return (
    <table className={'table table-hover'}>
      <TableHeader/>
      <tbody>
      { recipes.map(RecipeListItem) }
      </tbody>
    </table>
  )
}

function RecipeListItem(recipe, i) {
  return (
    <tr key={i}>
      <td>
        <Link to={`/recipe/${recipe.name}`}>
          { recipe.name }
        </Link>
      </td>
      <td>{ recipe.cookingTime }</td>
      <td>{ recipe.ingredients.join(', ') }</td>
    </tr>
  );
}
