import React from 'react';

export default function withPrefetch(WrappedComponent, dataFetch, dataPropKey = 'data') {
  return class extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        [dataPropKey]: null,
        loading: true
      };
    }

    componentDidMount() {
      dataFetch(this.props)
        .then(data => {
          this.setState({
            [dataPropKey]: { [dataPropKey]: data },
            loading: false
          })
        });
    }

    render() {
      if(this.state.loading) return null;

      return (
        <WrappedComponent
          {...this.state[dataPropKey]}
          {...this.props}
        />
      );
    }
  };
}
