import React from 'react';
import { Link } from "react-router-dom";

export default function Recipe({ recipe }) {
  if(!recipe) {
    return <p>Sorry, this recipe doesn't exist or may have been removed</p>
  }
  return (
    <div>
      <section>
        <h1>{recipe.name}</h1>
        <img src={recipe.imageUrl} alt={recipe.name} />
        <h3>Cooking Time</h3>
        <span>{recipe.cookingTime}</span>
        <h3>Ingredients</h3>
        <ul>
          { recipe.ingredients.map(Ingredient) }
        </ul>
      </section>
      <Link to='/'>All Recipes</Link>
    </div>
  );
}

function Ingredient(ingredient, i) {
  return <li key={i}>{ ingredient }</li>
}
