const defaultRecipes = require('../../test/fixtures/recipes.json');
let recipes = defaultRecipes.recipes;

function setRecipes(recipesArray) {
  recipes = recipesArray;
}

function getAllRecipes() {
  return recipes;
}

function findByName(name) {
  return recipes.find(recipe => {
    const recipeName = recipe.Name || recipe.name || '';
    return recipeName.toLowerCase() === name.toLowerCase()
  });
}

module.exports = {
  setRecipes,
  getAllRecipes,
  findByName
};