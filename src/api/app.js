const port = 3001;
const app = require('express')();
const recipes = require('./recipes');

app.get('/api/recipes', (req, res) => {
  res.json(recipes.getAllRecipes());
});

app.get('/api/recipe/:name', (req, res) => {
  const recipe = recipes.findByName(req.params.name);
  if(!recipe) {
    return res.sendStatus(404);
  }
  return res.json(recipe);
});

const server = app.listen(port, () => {
  // console.log(`recipe-api listening in on ${ port }`);
});

module.exports = { app, server };