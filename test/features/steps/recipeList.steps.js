import { Given, When, Then } from 'cucumber';
import { expect } from 'chai';
import _ from 'lodash';

import React from 'react';
import Recipes from '../../../src/web/src/components/Recipes';
import Adapter from 'enzyme-adapter-react-16';
import * as enzyme from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import testRecipes from '../../fixtures/recipes';

enzyme.configure({ adapter: new Adapter() });

Given('a recipe list', function() { });
When('there are no recipes in the system', function () { this.recipes = []; });
Then('the message {string} is displayed', function (message) {
  const renderedHtml = enzyme.render(<Recipes recipes={this.recipes} />).html();
  expect(renderedHtml).contains(message);
});

When('the following recipes exist in the system:', function (dataTable) {
  this.recipes = recipeDataTableToRecipes(dataTable);
});

Then('the recipe {string}', function(recipeName) {
  this.renderedHtml = enzyme.render(
    // memory router requires to provide context for react-dom-router links
    // (https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/guides/testing.md)
    <MemoryRouter>
      <Recipes recipes={this.recipes} />
    </MemoryRouter>
  ).html();
});

Then('the cooking time of {string}', function (cookingTime) {
  expect(this.renderedHtml).contains(cookingTime);
});

Then('the main ingredients are displayed:', function (dataTable) {
  const ingredients = flattenDataTableToString(dataTable);

  expect(this.renderedHtml).contains(ingredients);
});

When('a recipe is selected', function() {
  this.recipes = testRecipes.recipes.slice(0, 1);
});

Then('I am taken to the recipe page', function() {
  const wrapper = enzyme.render(
    <MemoryRouter>
      <Recipes recipes={this.recipes} />
    </MemoryRouter>
  )
  const lemonChickenLink = wrapper.find('a')[0].attribs.href;
  expect(lemonChickenLink).to.equal('/recipe/Lemon Chicken');

});

Then('the recipes along with their cooking time and main ingredients are displayed:', function (dataTable) {
  const wrapper = enzyme.render(
    <MemoryRouter>
      <Recipes recipes={this.recipes} />
    </MemoryRouter>
  );

  const renderedHtml = wrapper.html();

  const recipesToAssertOn = dataTable.rawTable;

  recipesToAssertOn.forEach(recipe => {
    recipe.forEach(recipeField => {
      expect(renderedHtml).contains(recipeField)
    })
  })

});

function flattenDataTableToString(dataTable) {
  return dataTable.rawTable.reduce((flattened, tableCell) => [ ...flattened, ...tableCell ], []).join(', ');
}

function recipeDataTableToRecipes(dataTable) {
  return dataTable.hashes().reduce((recipes, recipeHash) => [ ...recipes, recipeHashToRecipe(recipeHash) ] ,[])
}

function recipeHashToRecipe(recipeHash) {
  return Object.keys(recipeHash).reduce((recipe, key) => {

    let newKey = _.camelCase(key);
    let value;

    if(newKey === 'mainIngredients') {
      newKey = 'ingredients';
      value = recipeHash[key].split(',').map(_.trim);
    }

    recipe[newKey] = value || recipeHash[key];
    return recipe;
  }, {});
}