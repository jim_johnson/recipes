const assert = require('assert');
const request = require('supertest');
const { app, server } = require('../../src/api/app');
const recipes = require('../../src/api/recipes');
const testRecipes = require('../fixtures/recipes').recipes;

describe('Recipe api', () => {
  describe('/api/recipes', () => {
    describe('with no recipes', () => {
      it('returns empty array', done => {

        recipes.setRecipes([]);

        request(app)
          .get('/api/recipes')
          .expect(200)
          .then(res => {
            assert.deepEqual(res.body, []);
          })
          .then(done);
      })
    })
    describe('with 3 recipes', () => {
      it('returns an array of 3 recipes', done => {

        recipes.setRecipes(testRecipes);

        request(app)
          .get('/api/recipes')
          .expect(200)
          .then(res => {
            assert.deepEqual(res.body, testRecipes)
          })
          .then(done);
      })
    })
  });
  describe('/api/recipe/:name', () => {
    describe('with no recipes', () => {
      it('returns 404', done => {

        recipes.setRecipes([]);

        request(app)
          .get('/api/recipe/any-old-rubbish')
          .expect(404)
          .then(res => assert.equal(res.text, 'Not Found'))
          .then(done)
          .catch(done);
      })
    });
    describe('with 3 recipes', () => {
      it('returns the requested recipe', done => {

        recipes.setRecipes(testRecipes);

        request(app)
          .get('/api/recipe/lemon chicken')
          .expect(200)
          .then(res => {
            assert.deepEqual(
              res.body,
              {
                name: 'Lemon Chicken',
                imageUrl: 'http://ichef.bbci.co.uk/food/ic/food_16x9_608/recipes/lemonchicken_11654_16x9.jpg',
                ingredients: [ 'Chicken', 'Lemon', 'Thyme' ],
                cookingTime: '30 minutes'
              });
          })
          .then(done);
      })
    })
  });
  afterEach(() => {
    // tear down api server
    server.close();
  })
});
